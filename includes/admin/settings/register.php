<?php
/**
 * Register settings
 *
 * @package     Widgit\Portal\Settings\Register
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Setup the settings menu
 *
 * @since       1.0.0
 * @param       array $menu The default menu settings.
 * @return      array $menu Our defined settings
 */
function widgit_portal_add_menu( $menu ) {
	// TODO: This needs to be swappable depending on number of plugins installed.
	$menu['type']       = 'menu';
	$menu['page_title'] = __( 'Portal', 'widgit-portal' );
	$menu['menu_title'] = __( 'Portal', 'widgit-portal' );
	$menu['show_title'] = true;
	$menu['icon']       = 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDI1LjIuMSwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPgo8c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkxheWVyXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgoJIHZpZXdCb3g9IjAgMCA0NzUgNDc1IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0NzUgNDc1OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+CjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+Cgkuc3Qwe2ZpbGw6I0E1QUJCMDt9Cjwvc3R5bGU+CjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yMzcuNSw4LjRDMTEwLjEsNi4zLDI0LjMsMTQ5LjMsOTMuMSwyNTcuNmMzMi42LDU0LjMsOTguMSw4NC40LDE2MC44LDc3LjFDNDYxLjgsMzEzLjUsNDQ5LjksMTEsMjM3LjUsOC40egoJIE0yMzYuNCwyNzMuNGMtMTI5LTIuMi0xMjguOS0xOTYuNiwwLTE5OC44QzM2NS4zLDc2LjcsMzY1LjMsMjcxLjIsMjM2LjQsMjczLjR6Ii8+CjxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik0yOTUuNiwzOTMuOWMtNjUuNSwxOS40LTEzNi42LDAtMTk2LjQtMzEuOGMtMTMuNiwyMC4yLTIzLjYsMzgtMzEuMSw1NS41YzEwOCw2My41LDIzMy40LDYzLjMsMzM4LjcsMC4yCgljLTguOC0xOS45LTIxLjEtMzcuOS0zMS44LTU2LjhDMzUwLjQsMzc1LjIsMzIzLjcsMzg2LjIsMjk1LjYsMzkzLjl6Ii8+Cjwvc3ZnPgo=';

	return $menu;
}
add_filter( 'widgit_portal_menu', 'widgit_portal_add_menu' );


/**
 * Define our settings tabs
 *
 * @since       1.0.0
 * @param       array $tabs The default tabs.
 * @return      array $tabs Our defined tabs
 */
function widgit_portal_settings_tabs( $tabs ) {
	$tabs['core']    = __( 'Core', 'widgit-portal' );
	$tabs['support'] = __( 'Support', 'widgit-portal' );

	return $tabs;
}
add_filter( 'widgit_portal_settings_tabs', 'widgit_portal_settings_tabs' );


/**
 * Define settings sections
 *
 * @since       1.0.0
 * @param       array $sections The default sections.
 * @return      array $sections Our defined sections
 */
function widgit_portal_registered_settings_sections( $sections ) {
	// TODO: Make Welcome! only show on initial install and updates.
	$sections = array(
		'core'    => array(
			'welcome' => __( 'Welcome!', 'widgit-portal' ),
		),
		'support' => array(),
	);

	return $sections;
}
add_filter( 'widgit_portal_registered_settings_sections', 'widgit_portal_registered_settings_sections' );


/**
 * Disable save button on unsavable tabs
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function widgit_portal_define_unsavable_tabs() {
	$tabs = array( 'support' );

	return $tabs;
}
add_filter( 'widgit_portal_unsavable_tabs', 'widgit_portal_define_unsavable_tabs' );


/**
 * Disable save button on unsavable sections
 *
 * @since       1.0.0
 * @return      array $tabs The updated tabs
 */
function widgit_portal_define_unsavable_sections() {
	$sections = array( 'core/welcome' );

	return $sections;
}
add_filter( 'widgit_portal_unsavable_sections', 'widgit_portal_define_unsavable_sections' );


/**
 * Define our settings
 *
 * @since       1.0.0
 * @param       array $settings The default settings.
 * @return      array $settings Our defined settings
 */
function widgit_portal_registered_settings( $settings ) {
	$core_settings = array(
		'core'    => apply_filters(
			'widgit_portal_registered_settings_core',
			array(
				'welcome' => array(
					array(
						'id'   => 'welcome_header',
						'name' => '<h2>' . __( 'Welcome to Portal!', 'widgit-portal' ) . '</h2>',
						'desc' => '',
						'type' => 'header',
					),
					array(
						'id'   => 'welcome_about',
						'name' => __( 'What is Portal?', 'widgit-portal' ),
						'desc' => '',
						'type' => 'hook',
					),
				),
			),
		),
	);

	$core_settings = apply_filters( 'widgit_portal_registered_settings_modules', $core_settings );

	$support_settings = array(
		'support' => array(
			array(
				'id'   => 'support_header',
				'name' => __( 'Portal Support', 'widgit-portal' ),
				'desc' => '',
				'type' => 'header',
			),
			array(
				'id'   => 'system_info',
				'name' => __( 'System Info', 'widgit-portal' ),
				'desc' => '',
				'type' => 'sysinfo',
			),
		),
	);

	return array_merge( $settings, $core_settings, $support_settings );
}
add_filter( 'widgit_portal_registered_settings', 'widgit_portal_registered_settings' );


/**
 * Display our welcome text
 *
 * @since       1.0.0
 * @return      void
 */
function widgit_portal_welcome_about() {
	echo '<div style="max-width: 720px;">';

	$welcome_text = sprintf(
		// Translators: %s: WordPress site link.
		esc_html__( 'The Widgit hosting platform is built on a tweaked version of %s with a strict coding standard. Portal is the core of that platform, and is being built with the intention of providing a high-stability, modular platform for WordPress management.', 'widgit-portal' ),
		'<a href="https://wordpress.org/" target="_blank">' . __( 'WordPress', 'widgit-portal' ) . '</a>',
	);

	echo wp_kses_post( $welcome_text );

	echo '</div>';
}
add_action( 'widgit_portal_welcome_about', 'widgit_portal_welcome_about' );
