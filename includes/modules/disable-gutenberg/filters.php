<?php
/**
 * Filters
 *
 * @package     Widgit\Portal\Modules\DisableGutenberg\Filters
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Add module settings section
 *
 * @since       1.0.0
 * @param       array $sections The portal settings sections.
 * @return      array $sections The portal settings sections.
 */
function widgit_portal_registered_settings_sections_disable_gutenberg( $sections ) {
	$sections['core']['disable-gutenberg'] = __( 'Disable Gutenberg', 'widgit-portal' );

	return $sections;
}
add_filter( 'widgit_portal_registered_settings_sections', 'widgit_portal_registered_settings_sections_disable_gutenberg' );

/**
 * Add module settings
 *
 * @since       1.0.0
 * @param       array $settings The portal module settings.
 * @return      array $settings The portal module settings.
 */
function widgit_portal_registered_settings_disable_gutenberg( $settings ) {
	$settings['core']['disable-gutenberg'] = array(
		array(
			'id'   => 'disable_gutenberg_header',
			'name' => '<h2>' . __( 'Disable Gutenberg', 'widgit-portal' ) . '</h2>',
			'desc' => '',
			'type' => 'header',
		),
		array(
			'id'      => 'disable_gutenberg_for_post_types',
			'name'    => __( 'Disable for Post Types', 'widgit-portal' ),
			'desc'    => '',
			'type'    => 'multicheck',
			'options' => widgit_portal_disable_gutenberg_get_post_types(),
		),
		array(
			'id'      => 'disable_gutenberg_for_user_roles',
			'name'    => __( 'Disable for User Roles', 'widgit-portal' ),
			'desc'    => '',
			'type'    => 'multicheck',
			'options' => widgit_portal_disable_gutenberg_get_roles(),
		),
	);

	return $settings;
}
add_filter( 'widgit_portal_registered_settings_modules', 'widgit_portal_registered_settings_disable_gutenberg' );
