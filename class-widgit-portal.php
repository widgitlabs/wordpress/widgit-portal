<?php
/**
 * Portal
 *
 * @package     Widgit\Portal
 * @since       1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Widgit_Portal' ) ) {

	/**
	 * Main Portal class
	 *
	 * @access      public
	 * @since       1.0.0
	 */
	final class Widgit_Portal {

		/**
		 * The one true Widgit_Portal
		 *
		 * @access      private
		 * @since       1.0.0
		 * @var         Widgit_Portal $instance The one true Widgit_Portal
		 */
		private static $instance;

		/**
		 * The settings object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $settings The settings object
		 */
		public $settings;

		/**
		 * The modules object
		 *
		 * @access      public
		 * @since       1.0.0
		 * @var         object $modules The modules object
		 */
		public $modules;

		/**
		 * Get active instance
		 *
		 * @access      public
		 * @since       1.0.0
		 * @static
		 * @return      object self::$instance The one true Portal
		 */
		public static function instance() {
			if ( ! isset( self::$instance ) && ! ( self::$instance instanceof Widgit_Portal ) ) {
				self::$instance = new Widgit_Portal();
				self::$instance->setup_constants();
				self::$instance->includes();
				self::$instance->hooks();
			}

			return self::$instance;
		}

		/**
		 * Throw error on object clone
		 *
		 * The whole idea of the singleton design pattern is that there is
		 * a single object. Therefore, we don't want the object to be cloned.
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __clone() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'widgit-portal' ), '1.0.0' );
		}

		/**
		 * Disable unserializing of the class
		 *
		 * @access      protected
		 * @since       1.0.0
		 * @return      void
		 */
		public function __wakeup() {
			_doing_it_wrong( __FUNCTION__, esc_attr__( 'Cheatin&#8217; huh?', 'widgit-portal' ), '1.0.0' );
		}

		/**
		 * Setup plugin constants
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function setup_constants() {
			// Plugin version.
			if ( ! defined( 'WIDGIT_PORTAL_VER' ) ) {
				define( 'WIDGIT_PORTAL_VER', '1.0.0' );
			}

			// Plugin path.
			if ( ! defined( 'WIDGIT_PORTAL_DIR' ) ) {
				define( 'WIDGIT_PORTAL_DIR', plugin_dir_path( __FILE__ ) );
			}

			// Plugin URL.
			if ( ! defined( 'WIDGIT_PORTAL_URL' ) ) {
				define( 'WIDGIT_PORTAL_URL', plugin_dir_url( __FILE__ ) );
			}

			// Plugin file.
			if ( ! defined( 'WIDGIT_PORTAL_FILE' ) ) {
				define( 'WIDGIT_PORTAL_FILE', __FILE__ );
			}
		}

		/**
		 * Run plugin base hooks
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function hooks() {
			add_action( 'plugins_loaded', array( self::$instance, 'load_textdomain' ) );
		}

		/**
		 * Include necessary files
		 *
		 * @access      private
		 * @since       1.0.0
		 * @return      void
		 */
		private function includes() {
			/**
			 * TODO: Is this global actually needed?
			 *
			 * I know that it's dynamically generated in Simple Settings based
			 * on the `func` form of the instance slug, but I don't remember if
			 * I originally did that because I needed the global internally, or
			 * thought it would be common use. If it's not actually needed, the
			 * decision to create this should fall on the theme or plugin, not
			 * the library itself.
			 */
			global $widgit_portal_options;

			// Conditionally load the settings library.
			if ( ! class_exists( 'Simple_Settings' ) ) {
				require_once WIDGIT_PORTAL_DIR . 'vendor/widgitlabs/simple-settings/class-simple-settings.php';
			}

			require_once WIDGIT_PORTAL_DIR . 'includes/admin/settings/register.php';

			self::$instance->settings = new Simple_Settings( 'widgit-portal', 'core', array( 'sysinfo' ) );
			$widgit_portal_options    = self::$instance->settings->get_settings();

			// Load modules.
			require_once WIDGIT_PORTAL_DIR . 'includes/modules/disable-gutenberg/module.php';
		}

		/**
		 * Load plugin language files
		 *
		 * @access      public
		 * @since       1.0.0
		 * @return      void
		 */
		public function load_textdomain() {
			// Set filter for language directory.
			$lang_dir = dirname( plugin_basename( __FILE__ ) ) . '/languages/';
			$lang_dir = apply_filters( 'widgit_portal_languages_directory', $lang_dir );

			// WordPress plugin locale filter.
			$locale = apply_filters( 'plugin_locale', get_locale(), 'widgit-portal' );
			$mofile = sprintf( '%1$s-%2$s.mo', 'widgit-portal', $locale );

			// Setup paths to current locale file.
			$mofile_local  = $lang_dir . $mofile;
			$mofile_global = WP_LANG_DIR . '/widgit-portal/' . $mofile;
			$mofile_core   = WP_LANG_DIR . '/plugins/widgit-portal/' . $mofile;

			if ( file_exists( $mofile_global ) ) {
				// Look in global /wp-content/languages/widgit-portal folder.
				load_textdomain( 'widgit-portal', $mofile_global );
			} elseif ( file_exists( $mofile_local ) ) {
				// Look in local /wp-content/plugins/widgit-portal/languages/ folder.
				load_textdomain( 'widgit-portal', $mofile_local );
			} elseif ( file_exists( $mofile_core ) ) {
				// Look in core /wp-content/languages/plugins/widgit-portal/ folder.
				load_textdomain( 'widgit-portal', $mofile_core );
			} else {
				// Load the default language files.
				load_plugin_textdomain( 'widgit-portal', false, $lang_dir );
			}
		}
	}
}

/**
 * The main function responsible for returning the one true Widgit_Portal
 * instance to functions everywhere.
 *
 * Use this function like you would a global variable, except without
 * needing to declare the global.
 *
 * Example: <?php $portal = widgit_portal(); ?>
 *
 * @since       1.0.0
 * @return      Widgit_Portal The one true Widgit_Portal
 */
function widgit_portal() {
	return Widgit_Portal::instance();
}

// Get things started.
widgit_portal();
