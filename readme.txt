=== Portal ===
Contributors: evertiro
Tags: portal, widgit
Requires at least: 5.1
Tested up to: 5.7
Requires PHP: 7.2
Stable Tag: 1.0.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

The core of the Widgit platform.

== Description ==

Portal is the core of the Widgit platform. It provides extended logging and
debugging functionality, platform-wide helper functions, Widgit-specific UI
adjustjments, and the like. Portal is a must-use plugin within the Widgit
hosting environment, but can also be installed on any standard WordPress
site to add functionality to self-hosted sites. In a self-hosted environment,
each of the provided modules can be enabled at your discretion. Official Widgit
plugins may use Portal-specific helper functions, but will always provide a
fallback, allowing the plugin to work in a non-Portal environment.

== Installation ==

Portal is specifically designed to work in any WordPress-based environment.
Once downloaded, extract the `Portal` directory, upload it to your
`wp-content/plugins` directory, and activate the plugin.

If you want Portal to be treated as a must-use plugin, upload the `Portal`
directory to your `wp-content/mu-plugins` directory; creating `mu-plugins` if
necessary. Once uploaded, copy the `mu/class-portal-loader.php` file to the
`mu-plugins` directory.

== Changelog ==

## [1.0.0] - TBD
### Added
- First official release!